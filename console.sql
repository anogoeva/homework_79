DROP DATABASE IF EXISTS inventory;

CREATE DATABASE IF NOT EXISTS inventory;

use inventory;

CREATE TABLE IF NOT EXISTS categories (
    id int not null auto_increment primary key,
    name varchar(255) not null,
    description text null
);

CREATE TABLE IF NOT EXISTS locations (
    id int not null auto_increment primary key,
    name varchar(255) not null,
    description text null
);

CREATE TABLE IF NOT EXISTS accounting (
    id int not null  auto_increment primary key ,
    name varchar(255) not null,
    category_id int not null,
    location_id int not null,
    description text null,
    date timestamp null,

    constraint accounting_category_id_fk
    foreign key(category_id)
    references categories(id)
    on update cascade
    on delete restrict,

    constraint accounting_location_id_fk
    foreign key(location_id)
    references locations(id)
    on update cascade
    on delete restrict
);

INSERT INTO categories (name, description)
VALUES ('Мебель', 'Офисная'),
       ('Компьютерное оборудование', 'Устройства для ПК'),
       ('Бытовая техника', 'Крупная и мелкая');

INSERT INTO locations (name, description)
VALUES ('1 этаж', '3 кабинет'),
       ('2 этаж', '23 кабинет'),
       ('3 этаж', '33 кабинет');


INSERT INTO accounting (name, category_id, location_id, description, date)
VALUES ('Стул', 1, 1, 'Деревянный стул', '2019-07-02 06:14:00.742000000'),
       ('Стол', 1, 3, 'Стеклянный стол', '2020-07-02 06:14:00.742000000'),
       ('Диван', 1, 2, 'Кожанный диван', '2021-07-02 06:14:00.742000000'),
       ('Компьютер', 2, 1, 'Ноутбук', '2019-07-02 06:14:00.742000000'),
       ('Кондиционер', 3, 2, 'Зима-лето', '2021-08-02 08:20:00.742000000'),
       ('Микроволновка',3, 3, 'Samsung', '2020-02-02 06:14:00.742000000');

SELECT * FROM accounting;
