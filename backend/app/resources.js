const express = require('express');
const mysqlDb = require('../mysqlDb');

const router = express.Router();

router.get('/:resource', async (req, res) => {
  const resource = req.params.resource;
  const [resources] = await mysqlDb.getConnection().query('SELECT id, name FROM ??', [resource]);
  res.send(resources);
});

router.get('/:resource/:id', async (req, res) => {
  const resource = req.params.resource;
  const id = req.params.id;
  const [resources] = await mysqlDb.getConnection().query(
    'SELECT * FROM ?? where id = ?',
    [resource, id]);

  if (resources[0] === undefined) {
    return res.status(404).send({error: 'Accounting not found'});
  }

  res.send(resources[0]);
});

router.post('/:resource', async (req, res) => {
  const resource = req.params.resource;

  if (resource === 'accounting') {
    if (!req.body.name || !req.body.category_id || !req.body.location_id || !req.body.description) {
      return res.status(400).send({error: 'Data not valid'});
    }

    const accounting = {
      name: req.body.name,
      category_id: req.body.category_id,
      location_id: req.body.location_id,
      description: req.body.description,
      date: new Date().toISOString().slice(0, 19).replace('T', ' ')
    };

    const newAccounting = await mysqlDb.getConnection().query(
      'INSERT INTO ?? (name, category_id, location_id, description, date) values (?, ?, ?, ?, ?)',
      [resource, accounting.name, accounting.category_id, accounting.location_id, accounting.description, accounting.date]
    );
    res.send({
      ...accounting,
      id: newAccounting[0].insertId
    });
  } else if (resource === 'locations') {
    if (!req.body.name || !req.body.description) {
      return res.status(400).send({error: 'Data not valid'});
    }
    const locations = {
      name: req.body.name,
      description: req.body.description
    };
    const newLocation = await mysqlDb.getConnection().query(
      'INSERT INTO ?? (name, description) values (?, ?)',
      [resource, locations.name, locations.description]
    );
    res.send({
      ...locations,
      id: newLocation[0].insertId
    });
  } else if (resource === 'categories') {
    if (!req.body.name || !req.body.description) {
      return res.status(400).send({error: 'Data not valid'});
    }
    const categories = {
      name: req.body.name,
      description: req.body.description
    };
    const newCategories = await mysqlDb.getConnection().query(
      'INSERT INTO ?? (name, description) values (?, ?)',
      [resource, categories.name, categories.description]
    );
    res.send({
      ...categories,
      id: newCategories[0].insertId
    });
  }
});

router.put('/:resource/:id', async (req, res) => {
  const resource = req.params.resource;
  const id = req.params.id;

  if (resource === 'accounting') {
    const accounting = {
      name: req.body.name,
      category_id: req.body.category_id,
      location_id: req.body.location_id,
      description: req.body.description,
      date: new Date().toISOString().slice(0, 19).replace('T', ' ')
    };

    await mysqlDb.getConnection().query(
      'UPDATE ?? SET ? where id = ?',
      [resource, {...accounting}, id]);

    res.send({
      ...accounting,
      id: id
    });
  } else if (resource === 'locations') {
    const locations = {
      name: req.body.name,
      description: req.body.description
    };
    await mysqlDb.getConnection().query(
      'UPDATE ?? SET ? where id = ?',
      [resource, {...locations}, id]);

    res.send({
      ...locations,
      id: id
    });

  } else if (resource === 'categories') {
    const categories = {
      name: req.body.name,
      description: req.body.description
    };
    await mysqlDb.getConnection().query(
      'UPDATE ?? SET ? where id = ?',
      [resource, {...categories}, id]);

    res.send({
      ...categories,
      id: id
    });
  }
});

router.delete('/:resource/:id', async (req, res) => {
  const resource = req.params.resource;
  const id = req.params.id;
  try {
    const [newQuery] = await mysqlDb.getConnection().query(
      'DELETE FROM ?? where id = ?',
      [resource, id]);
    if (newQuery.affectedRows === 1) {
      res.send({message: 'Successfully deleted ' + newQuery.affectedRows + ' row with id ' + id});
    }
  } catch (error) {
    res.send({error: 'Unable to delete row with id ' + id + ', because there is a related entry.'});
  }
});

module.exports = router;
