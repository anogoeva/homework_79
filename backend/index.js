const express = require('express');
const resources = require('./app/resources');
const mysqlDb = require('./mysqlDb');

const app = express();
app.use(express.json());
const port = 8000;
app.use('/', resources);
mysqlDb.connect().catch(e => console.log(e));
app.listen(port, () => {
  console.log(`Server started on ${port} port!`);
});